<?php 
namespace JDWZ\Messages\Forms\PrayerRequest;

use JDWZ\Messages\Libs\AForm;

class Form extends AForm
{
	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formRules       = [
		'jdwz-fname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-lname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-email' => [
			'required' => true,
			'email' => true,
		],
		'jdwz-subject' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-request' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-specific' => [
			'required' => true,
			'min' => 5
		],
	];

	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formMessages    = [
		'jdwz-fname' => [
			'required' => 'Your first name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-lname' => [
			'required' => 'Your last name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-email' => [
			'required' => 'Your email is required',
			'email' => 'Your email address is invalid or not supported by our system'
		],
		'jdwz-subject' => [
			'required' => 'This field is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-request' => [
			'required' => 'This field is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-specific' => [
			'required' => 'This field is required',
			'min' => 'This field must be at least 5 characters long'
		],
	];

	/**
	 * Returns the form's name.
	 *
	 * @return string
	 */
	public function name()
	{
		return 'jdwz-msg-prayer-request';
	}

	/**
	 * Returns html string for rendering to screen.
	 *
	 * @param array $shortCodeAtts
	 * @param string $content
	 *
	 * @return string
	 */
	public function render($shortCodeAtts, $content = "")
	{
		if($this->isProcessed)
			return $this->formProcessedMessage();

		/**
		 * Set html for form.
		 */
		$this->html .= '<div class="jjdwz-msg-prayer-request-class" id="jdwz-form-container">';
		$this->html .= '<form method="POST" name="jdwz-msg-mailing-list" class="jdwz-msg-prayer-request" id="jdwz-msg-prayer-request">';
		$this->_formHtml();
		$this->sendButton("Submit");		
		$this->html .= '<div style="display: none;visibility: hidden;">';
		$this->html .= wp_nonce_field('jdwz-form');
		$this->html .= '<input type="hidden" name="jdwz-msg-form" value="jdwz-msg-prayer-request">';
		$this->html .= '</div>';
		$this->html .= '</form>';
		$this->html .= '</div>';

		/**
		 * Return html var.
		 */
		return $this->html;
	}

	/**
	 * Set form's short-code attributes.
	 *
	 * @param $shortCodeAtts
	 *
	 * @return void
	 */
	protected function setShortCodeAtts($shortCodeAtts) {}

	/**
	 * Generate form's html.
	 * 
	 * @return string
	 */
	private function _formHtml()
	{
		// Form title
		$this->html .= '<div class="jdwz-form-title">';
		$this->html .= '<h3 class="jdwz-form-title-class-h3" id="jdwz-form-title-id-h3">';
		$this->html .= 'Submit Prayer Your Request!';
		$this->html .= '</h3>';
		$this->html .= '</div>';

		// First name tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-fname">';
		$this->html .= '<label for="jdwz-fname" class="jdwz-fname-label" id="jdwz-fname-label">';
		$this->html .= 'First name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-fname" 
							class="jdwz-form-input" 
							id="jdwz-form-input-fname"
							value="'.$this->field('jdwz-fname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-fname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-fname').'</span>';
			$this->html .= '</div>';
		}

		// Last name tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-lname">';
		$this->html .= '<label for="jdwz-lname" class="jdwz-lname-label" id=jdwz-lname-label">';
		$this->html .= 'Last name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-lname" 
							class="jdwz-form-input" 
							id="jdwz-form-input-lname"
							value="'.$this->field('jdwz-lname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-lname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-lname').'</span>';
			$this->html .= '</div>';
		}

		// Email address tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-email">';
		$this->html .= '<label for="jdwz-email" class="jdwz-lname-label" id=jdwz-email-label">';
		$this->html .= 'Email address';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-email" 
							class="jdwz-form-input" 
							id="jdwz-form-input-email"
							value="'.$this->field('jdwz-email').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-email')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-email').'</span>';
			$this->html .= '</div>';
		}

		// Subject tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-email">';
		$this->html .= '<label for="jdwz-subject" class="jdwz-subject-label" id=jdwz-subject-label">';
		$this->html .= 'Subject';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-subject" 
							class="jdwz-form-input
							id="jdwz-form-input-subject"
							value="'.$this->field('jdwz-subject').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-subject')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-subject').'</span>';
			$this->html .= '</div>';
		}


		// Prayer request tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-request">';
		$this->html .= '<label for="jdwz-request" class="jdwz-subject-label" id=jdwz-request-label">';
		$this->html .= 'Prayer request';
		$this->html .= '</label>';
		$this->html .= '<textarea 
							type="text" 
							name="jdwz-request" 
							class="jdwz-form-input
							id="jdwz-form-input-request">';
		$this->html .= $this->field('jdwz-request');
		$this->html .= '</textarea>';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-request')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-request').'</span>';
			$this->html .= '</div>';
		}

		// Be specific tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-specific">';
		$this->html .= '<label for="jdwz-specific" class="jdwz-subject-label" id=jdwz-specific-label">';
		$this->html .= 'Be specific';
		$this->html .= '</label>';
		$this->html .= '<textarea 
							type="text" 
							name="jdwz-specific" 
							class="jdwz-form-input
							id="jdwz-form-input-specific">';
		$this->html .= $this->field('jdwz-specific');
		$this->html .= '</textarea>';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-specific')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-specific').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Return email template for admin.
	 *
	 * @return string
	 */
	public function adminEmailTemplate()
	{
		return $this->template(__FILE__, 'admin-email.html');
	}
}