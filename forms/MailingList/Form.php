<?php 
namespace JDWZ\Messages\Forms\MailingList;

use JDWZ\Messages\Libs\AForm;

class Form extends AForm
{
	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formRules       = [
		'jdwz-fname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-lname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-email' => [
			'required' => true,
			'email' => true,
		]
	];

	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formMessages    = [
		'jdwz-fname' => [
			'required' => 'Your first name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-lname' => [
			'required' => 'Your last name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-email' => [
			'required' => 'Your email is required',
			'email' => 'Your email address is invalid or not supported by our system'
		]
	];

	/**
	 * Returns the form's name.
	 *
	 * @return string
	 */
	public function name()
	{
		return 'jdwz-msg-mailing-list';
	}

	/**
	 * Returns html string for rendering to screen.
	 *
	 * @param array $shortCodeAtts
	 * @param string $content
	 *
	 * @return string
	 */
	public function render($shortCodeAtts, $content = "")
	{
		if($this->isProcessed)
			return $this->formProcessedMessage();

		/**
		 * Set html for form.
		 */
		$this->html .= '<div class="jdwz-msg-mailing-list-class" id="jdwz-form-container">';
		$this->html .= '<form method="POST" name="jdwz-msg-mailing-list" class="jdwz-msg-mailing-list" id="jdwz-msg-mailing-list">';
		$this->_formHtml();
		$this->sendButton("Subscribe", false);		
		$this->html .= '<div style="display: none;visibility: hidden;">';
		$this->html .= wp_nonce_field('jdwz-form');
		$this->html .= '<input type="hidden" name="jdwz-msg-form" value="jdwz-msg-mailing-list">';
		$this->html .= '<input type="checkbox" name="jdwz-mailinglist" checked="checked">';
		$this->html .= '</div>';
		$this->html .= '</form>';
		$this->html .= '</div>';

		/**
		 * Return html var.
		 */
		return $this->html;
	}

	/**
	 * Set form's short-code attributes.
	 *
	 * @param $shortCodeAtts
	 *
	 * @return void
	 */
	protected function setShortCodeAtts($shortCodeAtts) {}

	/**
	 * Generate form's html.
	 * 
	 * @return string
	 */
	private function _formHtml()
	{
		// Form title
		$this->html .= '<div class="jdwz-form-title">';
		$this->html .= '<h3 class="jdwz-form-title-class-h3" id="jdwz-form-title-id-h3">';
		$this->html .= 'Subscribe to our mailing list!';
		$this->html .= '</h3>';
		$this->html .= '</div>';

		// First name tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-fname">';
		$this->html .= '<label for="jdwz-fname" class="jdwz-fname-label" id="jdwz-fname-label">';
		$this->html .= 'First name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-fname" 
							class="jdwz-form-input" 
							id="jdwz-form-input-fname"
							value="'.$this->field('jdwz-fname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-fname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-fname').'</span>';
			$this->html .= '</div>';
		}

		// Last name tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-lname">';
		$this->html .= '<label for="jdwz-lname" class="jdwz-lname-label" id=jdwz-lname-label">';
		$this->html .= 'Last name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-lname" 
							class="jdwz-form-input" 
							id="jdwz-form-input-lname"
							value="'.$this->field('jdwz-lname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-lname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-lname').'</span>';
			$this->html .= '</div>';
		}

		// Email address tag
		$this->html .= '<div class="jdwz-form-input-container" id="jdwz-form-input-container-email">';
		$this->html .= '<label for="jdwz-email" class="jdwz-lname-label" id=jdwz-email-label">';
		$this->html .= 'Email address';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-email" 
							class="jdwz-form-input" 
							id="jdwz-form-input-email"
							value="'.$this->field('jdwz-email').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-email')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-email').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Return email template for admin.
	 *
	 * @return string
	 */
	public function adminEmailTemplate()
	{
		return $this->template(__FILE__, 'admin-email.html');
	}
}