<?php

namespace JDWZ\Messages\Forms\ContactForm;

use JDWZ\Messages\Libs\AForm;

class Form extends AForm
{
	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formRules       = [
		'jdwz-fname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-lname' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-email' => [
			'required' => true,
			'email' => true,
		],
		'jdwz-subject' => [
			'required' => true,
			'min' => 5
		],
		'jdwz-message' => [
			'required' => true,
			'min' => 20
		]
	];

	/**
	 * Form validation rules.
	 *
	 * @var array
	 */
	public $formMessages    = [
		'jdwz-fname' => [
			'required' => 'Your first name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-lname' => [
			'required' => 'Your last name is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-email' => [
			'required' => 'Your email is required',
			'email' => 'Your email address is invalid or not supported by our system'
		],
		'jdwz-subject' => [
			'required' => 'The message subject is required',
			'min' => 'This field must be at least 5 characters long'
		],
		'jdwz-message' => [
			'required' => 'The message is required',
			'min' => 'This field must be at least 20 characters long'
		]
	];

	/**
	 * Returns the form's name.
	 *
	 * @return string
	 */
	public function name()
	{
		return 'jdwz-msg-contact';
	}

	/**
	 * Returns html string for rendering to screen.
	 *
	 * @param array $shortCodeAtts
	 * @param string $content
	 *
	 * @return string
	 */
	public function render($shortCodeAtts, $content = "")
	{
		if($this->isProcessed)
			return $this->formProcessedMessage();


		// Set short-code attributes.
		$this->setShortCodeAtts($shortCodeAtts);

		/**
		 * Set html for form.
		 */
		$this->html .= '<div class="'.$this->atts['container-class'].'" id="'.$this->atts['container-id'].'">';
		$this->html .= '<form method="POST" name="jdwz-contact-form" class="jdwz-contact-form" id="jdwz-contact-form">';
		$this->_title();
		$this->_fnameField();
		$this->_lnameField();
		$this->_emailField();
		$this->_subjectField();
		$this->_messageField();
		$this->sendButton($this->atts['send-button-text']);		
		$this->html .= '<div style="display: none;visibility: hidden;">';
		$this->html .= wp_nonce_field('jdwz-form');
		$this->html .= '<input type="hidden" name="jdwz-msg-form" value="jdwz-msg-contact">';
		$this->html .= '</div>';
		$this->html .= '</form>';
		$this->html .= '</div>';

		/**
		 * Return html var.
		 */
		return $this->html;
	}

	/**
	 * Set form's short-code attributes.
	 *
	 * @param $shortCodeAtts
	 *
	 * @return void
	 */
	protected function setShortCodeAtts($shortCodeAtts)
	{
		$this->atts = shortcode_atts([
			'container-class' => '',
			'container-id' => 'jdwz-form-container',
			'title-text' => 'Contact Form',
			'title-id' => '',
			'title-class' => '',
			'title-p-id' => '',
			'title-p-class' => '',
			'name-id' => '',
			'name-class' => '',
			'name-label-text' => 'Your name',
			'name-label-class' => '',
			'name-label-id' => '',
			'name-input-class' => '',
			'name-input-id' => 'jdwz-name-field',
			'email-id' => '',
			'email-class' => '',
			'email-label-text' => 'Your email',
			'email-label-class' => '',
			'email-label-id' => '',
			'email-input-class' => '',
			'email-input-id' => 'jdwz-email-field',
			'subject-id' => '',
			'subject-class' => '',
			'subject-label-text' => 'Message subject',
			'subject-label-class' => '',
			'subject-label-id' => '',
			'subject-input-class' => '',
			'subject-input-id' => 'jdwz-subject-field',
			'message-id' => '',
			'message-class' => '',
			'message-label-text' => 'Your message',
			'message-label-class' => '',
			'message-label-id' => '',
			'message-input-class' => '',
			'message-input-id' => 'jdwz-message-field',
			'send-id' => 'jdwz-form-send-button-container',
			'send-class' => '',
			'send-button-text' => 'Submit',
			'send-button-class' => '',
			'send-button-id' => 'jdwz-send-button',
		], $shortCodeAtts);
	}

	/**
	 * Add title to html var.
	 */
	private function _title()
	{
		$this->html .= '<div class="'.$this->atts['title-class'].'" id="'.$this->atts['title-id'].'">';
		$this->html .= '<h3 class="'.$this->atts['title-p-class'].'" id="'.$this->atts['title-p-id'].'">';
		$this->html .= $this->atts['title-text'];
		$this->html .= '</h3>';
		$this->html .= '</div>';
	}

	/**
	 * Add first name field to html var.
	 */
	private function _fnameField()
	{
		$this->html .= '<div class="'.$this->atts['name-class'].'" id="'.$this->atts['name-id'].'">';
		$this->html .= '<label for="'.$this->atts['name-input-id'].'" class="'.$this->atts['name-label-class'].'" id="'.$this->atts['name-label-id'].'">';
		$this->html .= 'First name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-fname" 
							class="'.$this->atts['name-input-class'].'" 
							id="'.$this->atts['name-input-id'].'"
							value="'.$this->field('jdwz-fname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-fname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-fname').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Add last name field to html var.
	 */
	private function _lnameField()
	{
		$this->html .= '<div class="'.$this->atts['name-class'].'" id="'.$this->atts['name-id'].'">';
		$this->html .= '<label for="'.$this->atts['name-input-id'].'" class="'.$this->atts['name-label-class'].'" id="'.$this->atts['name-label-id'].'">';
		$this->html .= 'Last name';
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-lname" 
							class="'.$this->atts['name-input-class'].'" 
							id="'.$this->atts['name-input-id'].'"
							value="'.$this->field('jdwz-lname').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-lname')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-lname').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Add email field to the html var.
	 */
	private function _emailField()
	{
		$this->html .= '<div class="'.$this->atts['email-class'].'" id="'.$this->atts['email-id'].'">';
		$this->html .= '<label for="'.$this->atts['email-input-id'].'" class="'.$this->atts['email-label-class'].'" id="'.$this->atts['email-label-id'].'">';
		$this->html .= $this->atts['email-label-text'];
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-email" 
							class="'.$this->atts['email-input-class'].'" 
							id="'.$this->atts['email-input-id'].'"
							value="'.$this->field('jdwz-email').'"
							>';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-email')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-email').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Add subject field to the html var.
	 */
	private function _subjectField()
	{
		$this->html .= '<div class="'.$this->atts['subject-class'].'" id="'.$this->atts['subject-id'].'">';
		$this->html .= '<label for="'.$this->atts['subject-input-id'].'" class="'.$this->atts['subject-label-class'].'" id="'.$this->atts['subject-label-id'].'">';
		$this->html .= $this->atts['subject-label-text'];
		$this->html .= '</label>';
		$this->html .= '<input 
							type="text" 
							name="jdwz-subject" 
							class="'.$this->atts['subject-input-class'].'" 
							id="'.$this->atts['subject-input-id'].'"
							value="'.$this->field('jdwz-subject').'">';
		$this->html .= '</div>';

		if(!empty($this->hasError('jdwz-subject')))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-subject').'</span>';
			$this->html .= '</div>';
		}
	}

	/**
	 * Add the message field to the html var.
	 */
	private function _messageField()
	{
		$this->html .= '<div class="'.$this->atts['message-class'].'" id="'.$this->atts['message-id'].'">';
		$this->html .= '<label for="'.$this->atts['message-input-id'].'" class="'.$this->atts['message-label-class'].'" id="'.$this->atts['message-label-id'].'">';
		$this->html .= $this->atts['message-label-text'];
		$this->html .= '</label>';
		$this->html .= '<textarea 
							name="jdwz-message" 
							class="'.$this->atts['message-input-class'].'" 
							id="'.$this->atts['message-input-id'].'">';
		$this->html .= $this->field('jdwz-message');
		$this->html .= '</textarea>';
		$this->html .= '</div>';

		if($this->hasError('jdwz-message'))
		{
			$this->html .= '<div class="form-error">';
			$this->html .= '<span class="form-error-span">'.$this->error('jdwz-message').'</span>';
			$this->html .= '</div>';
		}
	}

	

	/**
	 * Return email template for admin.
	 *
	 * @return string
	 */
	public function adminEmailTemplate()
	{
		return $this->template(__FILE__, 'admin-email.html');
	}
}