<?php

namespace JDWZ\Messages\Libs;

use DrewM\MailChimp\MailChimp;
use JDWZ\Messages\Libs\AForm;
use DateTime;

class Processor
{
	/**
	 * Message table.
	 *
	 * @var string
	 */
	private $_messageTable;

	/**
	 * Token table.
	 *
	 * @var string
	 */
	private $_tokenTable;

	/**
	 * Form to process.
	 *
	 * @var AForm
	 */
	private $_form;

	/**
	 * Composed transport message.
	 *
	 * @var string
	 */
	private $_message;

	/**
	 * To email.
	 */
	private $_toEmail;

	/**
	 * Email subject.
	 *
	 * @var string
	 */
	private $_subject;

	/**
	 * Current date-time.
	 *
	 * @var string
	 */
	private $_now;

	/**
	 * Token.
	 *
	 * @var string
	 */
	private $_token;

	/**
	 * Key.
	 *
	 * @var int
	 */
	private $_key;

	/**
	 * Link.
	 */
	private $_link;

	/**
	 * Process post token values.
	 *
	 * @param \JDWZ\Messages\Libs\AForm $form
	 * @param array $token
	 *
	 * @return bool
	 */
	public function processGetToken(AForm &$form, $token)
	{
		add_filter('wp_mail_content_type', function() {
			return 'text/html';
		});

		// Set form.
		$this->_form = $form;

		$this->_setVars();

		$message = $this->_findMessage($token['message_id']);

		$this->_updateMessage($message['id']);
		$this->_updateToken($token['id']);

		if($message['mailing_list'] == 1)
			$this->_subscribeToMailChimp($message['from_email'], $message['from_fname'], $message['from_lname']);

		$data = unserialize($message['message']);
		$data['ip'] = $message['from_ip'];

		$this->_message = $this->_composeAdminMessage($form->adminEmailTemplate(), $data);
		$this->_toEmail = get_option('admin_email');
		$this->_subject = 'A message from ['.$form->name().']';

		// Send transport message.
		if($this->_form->transportMode == 'email')
			$this->_mail($this->_message);

		return true;
	}

	/**
	 * Process form data.
	 *
	 * @param \JDWZ\Messages\Libs\AForm $form
	 *
	 * @return mixed
	 */
	public function processPostData(AForm &$form)
	{
		add_filter('wp_mail_content_type', function() {
			return 'text/html';
		});

		$this->_form = $form;

		$this->_setVars();

		$this->_message = $this->_emailTemplate();
		$this->_toEmail = $this->_form->dataBag[$this->_form->fromFieldTransport];
		$this->_subject = "SOV [Message Security Link]";

		// Send transport message.
		if($this->_form->transportMode == 'email')
			$this->_mail($this->_emailTemplateTXT());

		$this->_insertToken($this->_insertMessage());

		// Set form as processed.
		$form->isProcessed = true;
		$form->processedStatus = Enums::$FormSuccess;
	}

	/**
	 * Mail message.
	 *
	 * @param string $txtVersion
	 */
	private function _mail($txtVersion="")
	{
		if($txtVersion != "")
		{
		    add_action('phpmailer_init', function($phpmailer) use($txtVersion) {
		    	$phpmailer->AltBody = $txtVersion;
		    });			
		}

		wp_mail($this->_toEmail, $this->_subject, $this->_message);
	}

	/**
	 * Set variables.
	 */
	private function _setVars()
	{
		global $wpdb;
		$this->_messageTable    = $wpdb->prefix . 'jdwz_messages';
		$this->_tokenTable    = $wpdb->prefix . 'jdwz_tokens';
		$this->_now             = current_time('mysql');
		$this->_key             = rand(100000,999999);
		$this->_token           = $this->_generateToken();
	}

	/**
	 * Find token.
	 *
	 * @param $urlToken
	 *
	 * @return array
	 */
	public function findToken($urlToken)
	{
		$this->_setVars();

		$token = explode('|', base64_decode($urlToken));

		if(count($token) != 2)
			return null;

		global $wpdb;
		$results = $wpdb->get_row("SELECT * FROM {$this->_tokenTable} WHERE `token`='{$token[0]}' AND `ukey`='{$token[1]}' AND `active`=1", ARRAY_A);

		if(empty($results))
			return null;

		if(time() > strtotime($results['expires']))
			return null;

		return $results;
	}

	/**
	 * Find message.
	 *
	 * @param $messageId
	 *
	 * @return array
	 */
	private function _findMessage($messageId)
	{
		global $wpdb;
		return $wpdb->get_row("SELECT * FROM {$this->_messageTable} WHERE `id`='{$messageId}'", ARRAY_A);
	}

	/**
	 * Update Message.
	 *
	 * @param $messageId
	 */
	private function _updateMessage($messageId)
	{
		global $wpdb;
		$wpdb->update(
			$this->_messageTable,
			[
				'date_sent' => $this->_now,
				'sent'  => 1
			],
			[
				'id' => $messageId
			]
		);
	}

	/**
	 * Deactivate token.
	 *
	 * @param $tokenId
	 */
	private function _updateToken($tokenId)
	{
		global $wpdb;
		$wpdb->update(
			$this->_tokenTable,
			[
				'active' => 0,
			],
			[
				'id' => $tokenId
			]
		);
	}

	/**
	 * Insert token
	 *
	 * @param $messageId
	 */
	private function _insertToken($messageId)
	{
		global $wpdb;
		$wpdb->insert(
			$this->_tokenTable,
			[
				'token'         => $this->_token,
				'ukey'          => $this->_key,
				'message_id'    => $messageId,
				'form_name'     => $this->_form->name(),
				'expires'       => (new DateTime("+ 15 minutes"))->format('Y-m-d H:i:s')
			]
		);

	}

	/**
	 * Insert message into the table.
	 *
	 * @return int
	 */
	private function _insertMessage()
	{
		global $wpdb;
		$wpdb->insert(
			$this->_messageTable,
			[				
				'from_fname'    => $this->_form->dataBag[$this->_form->fromFieldFName],
				'from_lname'    => $this->_form->dataBag[$this->_form->fromFieldLName],
				'from_email'	=> $this->_form->dataBag[$this->_form->fromFieldTransport],
				'message'       => serialize($this->_form->dataBag),
				'mailing_list'	=> (isset($_POST['jdwz-mailinglist']) && $_POST['jdwz-mailinglist'] == 'on') ? 1 : 0,
				'from_ip'       => $this->_getIP(),
				'date_created'  => $this->_now,
				'referrer'		=> filter_input(INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING)
			]
		);
		return $wpdb->insert_id;
	}

	/**
	 * Generate token.
	 *
	 * @param int $length
	 *
	 * @return bool|string
	 */
	private function _generateToken($length=15)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return substr(str_shuffle($chars), 0, $length);
	}

	/**
	 * Get user's up address.
	 */
	private function _getIP()
	{
		return !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "unknown";
	}

	/**
	 * Create a random string
	 *
	 * @author	XEWeb <>
	 * @param $length [the length of the string to create]
	 * @return string [the string]
	 */
	private function _randomString($length = 8) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	/**
	 * Create admin HTML message.
	 *
	 * @param string $template
	 * @param $data
	 *
	 * @return string
	 */
	private function _composeAdminMessage($template, $data)
	{
		$parentTemplate = file_get_contents(JDWZ_MSG_PATH . 'templates/admin-email.html');
		$template = str_replace('{body}', $template, $parentTemplate);

		foreach($data as $key => $value)
		{
			$replace = "%$key%";
			$template = str_replace($replace, $value, $template);
		}

		return $template;
	}

	/**
	 * Email HTML template with link.
	 *
	 * @return string
	 */
	private function _emailTemplate()
	{
		$this->_link = site_url(add_query_arg('jdwzSecurityToken', base64_encode($this->_token . '|' . $this->_key)));
		$template = file_get_contents(JDWZ_MSG_PATH . 'templates/security-email.html');
		return str_replace('{link}', $this->_link, $template);
	}

	/**
	 * Email HTML template with link.
	 *
	 * @return string
	 */
	private function _emailTemplateTXT()
	{
		$template = file_get_contents(JDWZ_MSG_PATH . 'templates/security-email.txt');
		return str_replace('{link}', $this->_link, $template);
	}


	/**
	 * Subscribe to mail chimp.
	 *
	 * @param string $email
	 * @param string $fname
	 * @param string $lname
	 */
	private function _subscribeToMailChimp($email, $fname, $lname)
	{
		$key = '1c8869f47082d6fdcc93d193b2c3500e-us17';
		$listId = '0598097e39';

		$MailChimp = new MailChimp($key);
		$MailChimp->post("lists/$listId/members", [
			'status'        => 'subscribed',
			'email_address' => $email,
			'merge_fields' 	=> [
				'FNAME'	=> $fname, 
				'LNAME'	=> $lname
			]
			
		]);
	}
}