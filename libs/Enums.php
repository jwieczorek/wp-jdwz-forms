<?php

namespace JDWZ\Messages\Libs;

abstract class Enums
{
	public static $FormSuccess          = 1;
	public static $FormValidationFailed = 2;
}