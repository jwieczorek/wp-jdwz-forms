<?php

namespace JDWZ\Messages\Libs;

class Validator
{
	use TValidators;

	/**
	 * Form.
	 *
	 * @var object
	 */
	protected $form;

	/**
	 * Process form data.
	 *
	 * @param $form
	 *
	 * @return void
	 */
	public function validate(&$form)
	{
		if(!$this->_verifyNonce())
		{
			add_filter('the_content', function() {
				return '<h3>Your session for this page has expired, please refresh the page and restart.</h3>';
			});
			$form->errorBag['wp_nonce'] = true;
		}
		else 
		{
			$this->form = $form;
			$this->_validatePostData();
		}
	}

	/**
	 * Verify wp-nonce
	 */
	private function _verifyNonce()
	{
		return wp_verify_nonce($_REQUEST['_wpnonce'], 'jdwz-form');	
	}

	/**
	 * Validate posted form data.
	 */
	private function _validatePostData()
	{
		foreach($this->form->formRules as $element => $rules)
		{
			$this->_validateRules($element, $rules);
		}
	}

	/**
	 * Validate form element rules.
	 *
	 * @param $element
	 * @param array $rules
	 */
	private function _validateRules($element, $rules=[])
	{
		foreach ($rules as $ruleName => $ruleValue)
		{
			$this->_validateRule($element, $ruleName, $ruleValue);
		}
	}

	/**
	 * Validate form element rule.
	 *
	 * @param $element
	 * @param $ruleName
	 * @param $ruleValue
	 */
	private function _validateRule($element, $ruleName, $ruleValue)
	{
		if(isset($this->form->errorBag[$element]))
			return;

		switch ($ruleName)
		{
			case 'required':
				$this->_validateRequired($element, $ruleName, $ruleValue);
				break;
			case 'email':
				$this->_validateEmail($element, $ruleName, $ruleValue);
				break;
			case 'min':
				$this->_validateMin($element, $ruleName, $ruleValue);
				break;
			case 'max':
				$this->_validateMax($element, $ruleName, $ruleValue);
				break;
		}
	}
}