<?php

namespace JDWZ\Messages\Libs;

abstract class AForm
{
	/**
	 * Form's submitter first name element.
	 *
	 * @var string
	 */
	public $fromFieldFName   = 'jdwz-fname';

	/**
	 * Form's submitter last name element.
	 *
	 * @var string
	 */
	public $fromFieldLName   = 'jdwz-lname';

	/**
	 * Form's submitter from transport [e.g. sms, email] element.
	 *
	 * @var string
	 */
	public $fromFieldTransport  = 'jdwz-email';

	/**
	 * Mode of transport [e.g. sms, email]
	 *
	 * @var string
	 */
	public $transportMode       = "email";

	/**
	 * Whether the form has been processed.
	 *
	 * @var bool
	 */
	public $isProcessed         = false;

	/**
	 * Form processed status.
	 *
	 * @var Enums
	 */
	public $processedStatus     = 0;

	/**
	 * Validation rules for the form elements.
	 *
	 * @var array
	 */
	public $formRules           = [];

	/**
	 * Form field validation messages.
	 *
	 * @var array
	 */
	public $formMessages        = [];

	/**
	 * Error messages
	 *
	 * @var array
	 */
	public $errorBag            = [];

	/**
	 * Posted form data bag.
	 *
	 * @var array
	 */
	public $dataBag             = [];

	/**
	 * Form's html.
	 *
	 * @var string
	 */
	protected $html             = '';

	/**
	 * Form's attributes.
	 *
	 * @var array
	 */
	protected $atts             = [];

	/**
	 * Returns name of form. [needs to be the same as the short-code]
	 *
	 * @return string
	 */
	abstract public function name();

	/**
	 * Returns html string for rendering to screen.
	 *
	 * @param array $shortCodeAtts
	 * @param string $content
	 *
	 * @return string
	 */
	abstract public function render($shortCodeAtts, $content="");

	/**
	 * Set form's short-code attributes.
	 *
	 * @param $shortCodeAtts
	 *
	 * @return void
	 */
	abstract protected function setShortCodeAtts($shortCodeAtts);

	/**
	 * Return email template for admin.
	 *
	 * @return string
	 */
	abstract public function adminEmailTemplate();

	/**
	 * Return whether the form has an error for a field.
	 *
	 * @param $field
	 *
	 * @return bool
	 */
	protected function hasError($field)
	{
		return isset($this->errorBag[$field]);
	}

	/**
	 * Return form field error message.
	 *
	 * @param $field
	 *
	 * @return string
	 */
	protected function error($field)
	{
		return $this->hasError($field) ? $this->errorBag[$field] : "";
	}

	/**
	 * Return form field data.
	 *
	 * @param $field
	 *
	 * @return string
	 */
	protected function field($field)
	{
		return isset($this->dataBag[$field]) ? $this->dataBag[$field] : "";
	}

	/**
	 * Return message if form has been processed.
	 *
	 * @return string
	 */
	protected function formProcessedMessage()
	{
		switch ($this->processedStatus)
		{
			case Enums::$FormSuccess;
				return $this->_msgFormSuccessful();
				break;
		}
	}

	/**
	 * Successful form post message.
	 *
	 * @return string
	 */
	private function _msgFormSuccessful()
	{
		return file_get_contents(JDWZ_MSG_PATH . 'templates/token-html.html');
	}

	/**
	 * Return SMS Template.
	 *
	 * @return string
	 */
	public function smsTemplate()
	{
		return "
			
		";
	}

	/**
	 * Add the send button to the html var
	 */
	protected function sendButton($buttonText="Submit", $showSubscribe=true)
	{
		$this->html .= '<div id="jdwz-form-send-button-container">';
		if($showSubscribe)
			$this->html .= '<p style="margin-top:5px"><label><input type="checkbox" name="jdwz-mailinglist" checked="checked"/> Subscribe to our mailing list?</label></p>';				
		$this->html .= '<input 
							type="submit" 
							name="jdwz-process" 
							id="'.$buttonId.'" 
							value="'.$buttonText.'"
						>';		
		$this->html .= '</div>';
	}

	/**
	 * Get a template file for a form.
	 *
	 * @param $template
	 * @param $file
	 */
	protected function template($template, $file)
	{
		if(file_exists(dirname($template) . '/Templates/' . $file))
			return file_get_contents(dirname($template) . '/Templates/' . $file);
		elseif(file_exists(dirname($template) . '/' . $file))
			return file_get_contents(dirname($template) . '/' . $file);
		return "";
	}
}