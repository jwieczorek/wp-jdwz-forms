<?php

namespace JDWZ\Messages\Libs;

/**
 * Contract for plugin tables.
 *
 * Interface ITable
 * @package JdwzMessages\Contracts
 */
interface ITable
{
	/**
	 * Return table version.
	 *
	 * @return string
	 */
	public function version();

	/**
	 * Return whether or not this db version
	 * already exists in the database.
	 *
	 * @return bool
	 */
	public function versionExists();

	/**
	 * Return db version table option name.
	 *
	 * @return string
	 */
	public function dbVersionOptionName();

	/**
	 * Update table db version option.
	 *
	 * @return void
	 */
	public function updateDBVersionOption();

	/**
	 * Return table name.
	 *
	 * @return string
	 */
	public function name();

	/**
	 * Return sql to create table.
	 *
	 * @param $charset_collate
	 *
	 * @return string
	 */
	public function createSql($charset_collate);

	/**
	 * Return sql to update table.
	 *
	 * @return string
	 */
	public function updateSql();
}