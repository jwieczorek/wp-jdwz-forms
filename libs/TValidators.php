<?php

namespace JDWZ\Messages\Libs;

trait TValidators
{
	/**
	 * Validate element is required.
	 *
	 * @param $element
	 * @param $ruleName
	 * @param $ruleValue
	 */
	protected function _validateRequired($element, $ruleName, $ruleValue)
	{
		$value = filter_input(INPUT_POST, $element, FILTER_SANITIZE_STRING);

		$this->form->dataBag[$element] = $_POST[$element];

		if(empty($value) && $ruleValue == true)
			$this->form->errorBag[$element] = $this->form->formMessages[$element][$ruleName];
	}

	/**
	 * Validate element is valid email.
	 *
	 * @param $element
	 * @param $ruleName
	 * @param $ruleValue
	 */
	protected function _validateEmail($element, $ruleName, $ruleValue)
	{
		$value = filter_input(INPUT_POST, $element, FILTER_VALIDATE_EMAIL);

		$this->form->dataBag[$element] = $_POST[$element];

		if(empty($value) && $ruleValue == true)
			$this->form->errorBag[$element] = $this->form->formMessages[$element][$ruleName];
	}

	/**
	 * Validate element min length.
	 *
	 * @param $element
	 * @param $ruleName
	 * @param $ruleValue
	 */
	protected function _validateMin($element, $ruleName, $ruleValue)
	{
		$value = filter_input(INPUT_POST, $element, FILTER_SANITIZE_STRING);

		$this->form->dataBag[$element] = $_POST[$element];

		if(empty($value) || strlen($value) < $ruleValue)
			$this->form->errorBag[$element] = $this->form->formMessages[$element][$ruleName];
	}

	/**
	 * Validate element max length.
	 *
	 * @param $element
	 * @param $ruleName
	 * @param $ruleValue
	 */
	protected function _validateMax($element, $ruleName, $ruleValue)
	{
		$value = filter_input(INPUT_POST, $element, FILTER_SANITIZE_STRING);

		$this->form->dataBag[$element] = $_POST[$element];

		if(empty($value) || strlen($value) > $ruleValue)
			$this->form->errorBag[$element] = $this->form->formMessages[$element][$ruleName];
	}
}