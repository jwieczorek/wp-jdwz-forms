<?php

namespace JDWZ\Messages\Libs;

/**
 * Core table class.
 *
 * Class ATable
 * @package JdwzMessages\Contracts
 */
class Table
{
	/**
	 * Table prefix.
	 *
	 * @var string
	 */
	public $tablePrefix;

	/**
	 * Option name prefix.
	 *
	 * @var string
	 */
	protected $_dbVersionOptionSuffix = 'DBVersion';
}