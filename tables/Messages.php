<?php

namespace JDWZ\Messages\Tables;

use JDWZ\Messages\Libs\ITable;
use  JDWZ\Messages\Libs\Table;

class Messages extends Table implements ITable
{
	/**
	 * Return table version.
	 *
	 * @return string
	 */
	public function version()
	{
		return '1.0';
	}

	/**
	 * Return whether or not this db version
	 * already exists in the database.
	 *
	 * @return bool
	 */
	public function versionExists()
	{
		return $this->version() == get_option($this->dbVersionOptionName());
	}

	/**
	 * Get db version option name.
	 *
	 * @return string
	 */
	public function dbVersionOptionName()
	{
		return ltrim($this->name(), $this->tablePrefix) . $this->_dbVersionOptionSuffix;
	}

	/**
	 * Update table db version option.
	 *
	 * @return void
	 */
	public function updateDBVersionOption()
	{
		update_option($this->dbVersionOptionName(), $this->version());
	}

	/**
	 * Return table name.
	 *
	 * @return string
	 */
	public function name()
	{
		return $this->tablePrefix . 'jdwz_messages';
	}

	/**
	 * Return sql to create table.
	 *
	 * @param $charset_collate
	 *
	 * @return string
	 */
	public function createSql($charset_collate)
	{
		return "
			CREATE TABLE {$this->name()} (
				id BIGINT NOT NULL AUTO_INCREMENT,				
				message LONGTEXT NOT NULL,
				from_fname VARCHAR(255) NOT NULL,
				from_lname VARCHAR(255) NOT NULL,
				from_email VARCHAR(255) NOT NULL,
				from_ip VARCHAR(100) NOT NULL,
				referrer VARCHAR(150) NULL,
				sent TINYINT(1) DEFAULT 0 NOT NULL,
				mailing_list TINYINT(1) DEFAULT 0 NOT NULL,
				date_created DATETIME NOT NULL,
				date_sent DATETIME NULL, 
				PRIMARY KEY  (id)
			) {$charset_collate}";
	}

	/**
	 * Return sql to update table.
	 */
	public function updateSql()
	{
		return "";
	}
}