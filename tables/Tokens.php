<?php

namespace JDWZ\Messages\Tables;

use JDWZ\Messages\Libs\ITable;
use  JDWZ\Messages\Libs\Table;

class Tokens extends Table implements ITable
{
	/**
	 * Return table version.
	 *
	 * @return string
	 */
	public function version()
	{
		return '1.0';
	}

	/**
	 * Return whether or not this db version
	 * already exists in the database.
	 *
	 * @return bool
	 */
	public function versionExists()
	{
		return $this->version() == get_option($this->dbVersionOptionName());
	}

	/**
	 * Get db version option name.
	 *
	 * @return string
	 */
	public function dbVersionOptionName()
	{
		return ltrim($this->name(), $this->tablePrefix) . $this->_dbVersionOptionSuffix;
	}

	/**
	 * Update table db version option.
	 *
	 * @return void
	 */
	public function updateDBVersionOption()
	{
		update_option($this->dbVersionOptionName(), $this->version());
	}

	/**
	 * Return table name.
	 *
	 * @return string
	 */
	public function name()
	{
		return $this->tablePrefix . 'jdwz_tokens';
	}

	/**
	 * Return sql to create table.
	 *
	 * @param $charset_collate
	 *
	 * @return string
	 */
	public function createSql($charset_collate)
	{
		return "
			CREATE TABLE {$this->name()} (
				id BIGINT NOT NULL AUTO_INCREMENT,				
				token VARCHAR(15) NOT NULL,
				ukey INT(6) NOT NULL,
				message_id BIGINT NOT NULL, 
				form_name VARCHAR(50) NOT NULL,
				active INT(1) DEFAULT 1 NOT NULL,
				expires DATETIME NOT NULL,
				PRIMARY KEY  (id)
			) {$charset_collate}";
	}

	/**
	 * Return sql to update table.
	 */
	public function updateSql()
	{
		return "";
	}
}