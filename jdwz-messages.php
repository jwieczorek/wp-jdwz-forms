<?php
/*
Plugin Name: Jdwz Messages
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A form plugin that is easily customizable.
Version: 1.0
Author: Joshua Wieczorek
Author URI: http://www.joshuawieczorek.com
License: A "Slug" license name e.g. GPL2
*/

class JdwzMessagesPlugin
{
	/**
	 * Plugin's tables.
	 *
	 * @var array
	 */
	private $_tables = [];

	/**
	 * Plugin's forms.
	 *
	 * @var array
	 */
	private $_forms = [];

	/**
	 * Active form.
	 *
	 * @var string
	 */
	private $_activeForm = "";

	/**
	 * Form validator.
	 *
	 * @var JDWZ\Messages\Libs\Validator
	 */
	private $_validator;

	/**
	 * Form processor.
	 *
	 * @var JDWZ\Messages\Libs\Processor
	 */
	private $_processor;

	/**
	 * Public function called by wordpress init hook
	 * to load and run this plugin.
	 */
	public function load()
	{
		$this->_setVars();
		$this->_loadFiles();
		$this->_setTables();
		$this->_setForms();
		$this->_run();
		$this->_addShortCodes();

		// Form validator.
		$this->_validator = new JDWZ\Messages\Libs\Validator();

		// Form processor.
		$this->_processor = new JDWZ\Messages\Libs\Processor();
	}

	/**
	 * Set global variables.
	 */
	public function _setVars()
	{
		defined('JDWZ_MSG_PATH') || define('JDWZ_MSG_PATH', plugin_dir_path(__FILE__) . '/');
		defined('JDWZ_MSG_URL') || define('JDWZ_MSG_URL', plugin_dir_url( __FILE__ ));
	}

	/**
	 * Load files.
	 */
	private function _loadFiles()
	{
		include JDWZ_MSG_PATH . 'vendor/autoload.php';
		include JDWZ_MSG_PATH . 'libs/Enums.php';
		include JDWZ_MSG_PATH . 'libs/AForm.php';
		include JDWZ_MSG_PATH . 'libs/ITable.php';
		include JDWZ_MSG_PATH . 'libs/TValidators.php';
		include JDWZ_MSG_PATH . 'libs/Validator.php';
		include JDWZ_MSG_PATH . 'libs/Processor.php';
		include JDWZ_MSG_PATH . 'libs/Table.php';
		include JDWZ_MSG_PATH . 'tables/Messages.php';
		include JDWZ_MSG_PATH . 'tables/Tokens.php';
		include JDWZ_MSG_PATH . 'forms/ContactForm/Form.php';
		include JDWZ_MSG_PATH . 'forms/MailingList/Form.php';
		include JDWZ_MSG_PATH . 'forms/PrayerRequest/Form.php';
	}

	/**
	 * Set tables.
	 */
	private function _setTables()
	{
		$this->_tables = [
			JDWZ\Messages\Tables\Messages::class,
			JDWZ\Messages\Tables\Tokens::class
		];
	}

	/**
	 * Set forms.
	 */
	private function _setForms()
	{
		$this->_forms = [
			'jdwz-msg-contact' => new JDWZ\Messages\Forms\ContactForm\Form(),
			'jdwz-msg-mailing-list'	=> new JDWZ\Messages\Forms\MailingList\Form(),
			'jdwz-msg-prayer-request'	=> new JDWZ\Messages\Forms\PrayerRequest\Form(),
		];
	}

	/**
	 * Run plugin.
	 */
	private function _run()
	{
		if(isset($_GET['jdwzSecurityToken']) && !empty($_GET['jdwzSecurityToken']))
			add_action('init', [$this, 'processToken']);
		else
			add_action('init', [$this, 'processForm']);
	}

	/**
	 * Process get token.
	 */
	public function processToken()
	{
		$token = $this->_processor->findToken($_GET['jdwzSecurityToken']);

		if($token == null)
		{
			add_filter('the_content', function() {
				return "<h2>The the link is either broken or expired.</h2>";
			});
		}
		else
		{
			$this->_processor->processGetToken(
				$this->_forms[$token['form_name']],
				$token
			);


			if($this->_forms[$token['form_name']]->name() == 'jdwz-msg-mailing-list')
			{
				add_filter('the_content', function() {
					return "<h2>You have been successfully subcribed.</h2>";
				});	
			}
			else
			{
				add_filter('the_content', function() {
					return "<h2>Your message has been successfully sent.</h2>";
				});	
			}			
		}
	}

	/**
	 * Process posted form data.
	 */
	public function processForm()
	{
		if(isset($_POST['jdwz-msg-form']) && isset($this->_forms[$_POST['jdwz-msg-form']]))
		{
			$form = $this->_forms[$_POST['jdwz-msg-form']];

			$this->_validator->validate($form);

			if(count($form->errorBag) == 0)
				$this->_processor->processPostData($form);
		}
	}

	/**
	 * Setup short-codes form forms.
	 */
	private function _addShortCodes()
	{
		foreach($this->_forms as $shortCode => $form)
		{
			add_shortcode($shortCode, [$form, 'render']);
		}
	}

	/**
	 * Install tables and setup configuration values.
	 */
	public function install()
	{
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		foreach ($this->_tables as $table)
		{
			$tableClass = new $table();
			$tableClass->tablePrefix = $wpdb->prefix;

			if($tableClass->versionExists() != true)
			{
				dbDelta($tableClass->createSql($charset_collate));
				$tableClass->updateDBVersionOption();
			}
		}
	}

	/**
	 * Load css for forms.
	 */
	public function insertCSS()
	{
		wp_enqueue_style( 'jdwz-msg-styles', JDWZ_MSG_URL . 'css/style.css' );
	}
}

/**
 * Instantiate and load plugin.
 */
$jdwzMessagePlugin = new JdwzMessagesPlugin();
$jdwzMessagePlugin->load();


/**
 * Add css scripts.
 */
add_action('wp_enqueue_scripts', [$jdwzMessagePlugin, 'insertCSS']);

/**
 * Install plugin.
 */
register_activation_hook(__FILE__, [$jdwzMessagePlugin, 'install']);